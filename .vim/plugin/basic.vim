set nocompatible
set number
set showcmd
set linebreak
set showbreak=→\ 

set autoindent
set smartindent

set textwidth=80
set tabstop=4
set expandtab
set shiftwidth=4

set hlsearch
set incsearch

set noswapfile

filetype plugin indent on
syntax on
