colorscheme pablo

" Colore les espaces insécables
highlight NbSp ctermbg=darkgray guibg=lightred
match NbSp /\%xa0/
set listchars=nbsp:░,tab:\ \ 
set list
